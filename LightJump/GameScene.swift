//
//  GameScene.swift
//  LightJump
//
//  Created by Nikola on 03/05/2018.
//  Copyright © 2018 Nikola Bozhkov. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var ui: GameUI!
    
    var kills = 0
    var moves = 0
    
    let lifeStackDuration: TimeInterval = 0.2
    let lifePerStackPerSecond: CGFloat = 74
    var lifeStacks: CGFloat = 0
    
    let lifeGain: CGFloat = 16
    var lifeLoss: CGFloat = 50
    var life: CGFloat = 100 {
        didSet {
            life.clamp(0, 100)
            ui.lifeDidChange(life)
        }
    }
    
    var player: Player!
    var gameField: GameField!
    var spawner: Spawner!
    var lastTime: TimeInterval = 0
    var gameOver = false
    
    var pointsQueue = [SKShapeNode]()
    var currentPoint: SKShapeNode!
    var stage: CGFloat = 1 {
        didSet {
            ui.stageDidChange(stage)
        }
    }
    
    override func didMove(to view: SKView) {
        super.didMove(to: view)
        physicsWorld.contactDelegate = self
        
        gameField = GameField(size: size)
        spawner = Spawner(scene: self)
        ui = GameUI(scene: self)
        
        currentPoint = gameField.points.randomElement()
        currentPoint.fillColor = .red
        
        player = Player(position: currentPoint.position)
        player.emitter.targetNode = self
        
        addChild(gameField)
        addChild(player)
        
        for _ in 0..<11 {
            spawner.spawnEnemy()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let location = touches.first?.location(in: self) else { return }
        
        if gameOver, let scene = GameScene.create(for: view) {
            view?.presentScene(scene)
            return
        }
        
        var targetPoint: SKShapeNode!
        if location.x < gameField.midX && location.y > gameField.midY {
            targetPoint = gameField.pointTopLeft
        }
        else if location.x > gameField.midX && location.y > gameField.midY {
            targetPoint = gameField.pointTopRight
        }
        else if location.x < gameField.midX && location.y < gameField.midY {
            targetPoint = gameField.pointBottomLeft
        }
        else if location.x > gameField.midX && location.y < gameField.midY {
            targetPoint = gameField.pointBottomRight
        }
        
        let targetingCurrent = targetPoint == currentPoint && pointsQueue.isEmpty
        if !targetingCurrent && targetPoint != pointsQueue.last {
            queue(point: targetPoint)
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        let deltaT = lastTime == 0 ? 0 : currentTime - lastTime
        guard !gameOver else {
            lastTime = currentTime
            return
        }
        
        life += lifeStacks * lifePerStackPerSecond * CGFloat(deltaT)
        
        ui.update(deltaT: deltaT)
        spawner.update(deltaT: deltaT)
        updateShouldEndGame()
        
        lastTime = currentTime
    }
    
    // MARK: CUSTOM
    func updateShouldEndGame() {
        guard life <= 0 else { return }
        print(spawner.spawnInterval)
        gameOver = true
        print(CGFloat(kills) / CGFloat(moves))
        
        player.removeFromParent()
        let label = SKLabelNode(text: "Game Over")
        label.fontSize = 54
        label.fontColor = .white
        label.position = CGPoint(x: frame.midX, y: frame.maxY - label.frame.height / 2 - 300)
        addChild(label)
        
        let label1 = label.copy() as! SKLabelNode
        label1.text = "Tap to Restart"
        label1.position = label.position.offsetted(dx: 0, dy: -label.frame.height / 2 - label1.frame.height / 2 - 20)
        addChild(label1)
    }
    
    func move(to point: SKShapeNode) {
        moves += 1
        
        let durationForWidth: TimeInterval = 2
        let speedModifier = 7.5 / pow(stage + 3.7, 1.3) // slow deceleration curve (lowers duration)
        let durationToMove = durationForWidth
            * TimeInterval((player.position.distanceTo(point.position) / gameField.width) * speedModifier)
        
        player.run(SKAction.sequence([
            SKAction.run {
                self.currentPoint?.fillColor = .white
            },
            SKAction.move(to: point.position,
                          duration: durationToMove),
            SKAction.run {
                point.fillColor = .red
                self.currentPoint = point
            },
            SKAction.run {
                self.pointsQueue.removeFirst()
                if let nextPoint = self.pointsQueue.first {
                    self.move(to: nextPoint)
                }
            }]))
        
        var lastTime: CGFloat = 0
        run(SKAction.customAction(withDuration: durationToMove, actionBlock: { (node, timePassed) in
            let delta = timePassed - lastTime
            self.life -= (delta / CGFloat(durationToMove)) * self.lifeLoss
            lastTime = timePassed
        }))
    }
    
    func queue(point: SKShapeNode) {
        pointsQueue.append(point)
        
        if pointsQueue.count == 1 {
            stage = 1
            move(to: point)
        } else if pointsQueue.count > 2 {
            stage += 1
        }
    }
}

// MARK: SKPhysicsContactDelegate
extension GameScene: SKPhysicsContactDelegate {
    
    func didBegin(_ contact: SKPhysicsContact) {
        let enemy = contact.bodyA.node! is Enemy ? contact.bodyA.node! as! Enemy : contact.bodyB.node! as! Enemy
        enemy.removeFromParent()
        spawner.spawnEnemy(except: enemy.spawnPoint)
        
        kills += 1
        ui.didKillEnemy()
        
        lifeStacks += 1
        self.run(SKAction.sequence([
            SKAction.wait(forDuration: lifeStackDuration),
            SKAction.run {
                self.lifeStacks -= 1
            }]))
        
        // little effects
        let particlesCount = 3 + arc4random_uniform(3)
        for _ in 0..<particlesCount {
            let radiusScale = CGFloat.random(min: 0.6, max: 0.8)
            let particle = SKShapeNode(circleOfRadius: Enemy.radius * radiusScale)
            particle.position = enemy.position
            particle.fillColor = enemy.fillColor
            particle.lineWidth = 0
            
            particle.run(SKAction.sequence([
                SKAction.scale(to: 0, duration: TimeInterval(radiusScale * 1)),
                SKAction.removeFromParent()]))
            
            let rotation = CGFloat.random(min: 0, max: .pi * 2)
            let distance = CGFloat.random(min: Enemy.radius * 3, max: Enemy.radius * 5)
            let offset = CGVector(dx: distance * cos(rotation), dy: distance * sin(rotation))
            particle.run(SKAction.move(by: offset, duration: TimeInterval(offset.length / Enemy.radius / 3)))
            
            addChild(particle)
        }
    }
}
