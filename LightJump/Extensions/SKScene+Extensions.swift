//
//  SKScene+Extensions.swift
//  LightJump
//
//  Created by Nikola Bozhkov on 2/23/19.
//  Copyright © 2019 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

extension SKScene {
    
    
    /// Creates an SKScene from the file named as the type of the caller
    ///
    /// - Parameter view: The view the scene will be presented in
    /// - Returns: The initialized scene of the caller's type
    class func create(for view: SKView?) -> Self? {
        guard let view = view else { return nil }
        return createHelper(for: view)
    }
    
    private class func createHelper<T: SKScene>(for view: SKView) -> T? {
        guard let scene = SKScene(fileNamed: "\(self)") as? T else { return nil }
        let newHeight = round((view.frame.height / view.frame.width) * scene.size.width)
        scene.size = CGSize(width: scene.size.width, height: newHeight)
        scene.scaleMode = .aspectFill
        return scene
    }
    
    /// Returns a point in scene coordinates from the
    /// top-left corner of the view respecting safeAreaInsets
    ///
    /// - Parameters:
    ///   - left: The left position offset
    ///   - top: The top position offset
    /// - Returns: The point in scene coordinates
    func point(left: CGFloat, top: CGFloat) -> CGPoint {
        guard let view = view else { return .zero }
        let topLeft = CGPoint(x: view.safeAreaInsets.left, y: view.safeAreaInsets.top)
        return convertPoint(fromView: topLeft) + CGPoint(x: left, y: -top)
    }
    
    /// Returns a point in scene coordinates from the
    /// bottom-right corner of the view respecting safeAreaInsets
    ///
    /// - Parameters:
    ///   - right: The right position offset
    ///   - bottom: The bottom position offset
    /// - Returns: The point in scene coordinates
    func point(right: CGFloat, bottom: CGFloat) -> CGPoint {
        guard let view = view else { return .zero }
        let bottomRight = CGPoint(x: view.bounds.width - view.safeAreaInsets.right,
                                  y: view.bounds.height - view.safeAreaInsets.bottom)
        return convertPoint(fromView: bottomRight) + CGPoint(x: -right, y: bottom)
    }
    
    /// Returns a point in scene coordinates from the
    /// bottom-left corner of the view respecting safeAreaInsets
    ///
    /// - Parameters:
    ///   - left: The left position offset
    ///   - bottom: The bottom position offset
    /// - Returns: The point in scene coordinates
    func point(left: CGFloat, bottom: CGFloat) -> CGPoint {
        guard let view = view else { return .zero }
        let bottomLeft = CGPoint(x: view.safeAreaInsets.left,
                                 y: view.bounds.height - view.safeAreaInsets.bottom)
        return convertPoint(fromView: bottomLeft) + CGPoint(x: left, y: bottom)
    }
    
    /// Returns a point in scene coordinates from the
    /// top-right corner of the view respecting safeAreaInsets
    ///
    /// - Parameters:
    ///   - right: The right position offset
    ///   - top: The top position offset
    /// - Returns: The point in scene coordinates
    func point(right: CGFloat, top: CGFloat) -> CGPoint {
        guard let view = view else { return .zero }
        let topRight = CGPoint(x: view.bounds.width - view.safeAreaInsets.right,
                               y: view.safeAreaInsets.top)
        return convertPoint(fromView: topRight) + CGPoint(x: -right, y: -top)
    }
    
    func left(_ x: CGFloat) -> CGFloat {
        return point(left: x, top: 0).x
    }
    
    func right(_ x: CGFloat) -> CGFloat {
        return point(right: x, top: 0).x
    }
    
    func top(_ y: CGFloat) -> CGFloat {
        return point(left: 0, top: y).y
    }
    
    func bottom(_ y: CGFloat) -> CGFloat {
        return point(left: 0, bottom: y).y
    }
}
