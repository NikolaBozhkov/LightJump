//
//  CGSize+Extensions.swift
//  LightJump
//
//  Created by Nikola Bozhkov on 23.02.19.
//  Copyright © 2019 Nikola Bozhkov. All rights reserved.
//

import CoreGraphics

extension CGSize {
    static func *(lhs: CGSize, rhs: CGFloat) -> CGSize {
        return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
    }
}
