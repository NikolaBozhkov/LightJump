//
//  SKLabelNode+Extensions.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/11/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

extension SKLabelNode {
    func animate(start: Int, delta: Int, timingMode: SKActionTimingMode, format: String, duration: CGFloat) throws {
        
        let formatSplit = format.components(separatedBy: "(*)")
        
        if formatSplit.count < 2 {
            throw FormatError.wrongFormat("expected '(*)'")
        }
        
        let animation = SKAction.customAction(withDuration: TimeInterval(duration), actionBlock: {(label, timePassed) in
            let currValue = start + Int(CGFloat(delta) * timePassed / duration)
            self.text = formatSplit[0] + String(currValue) + formatSplit[1]
        })
        
        animation.timingMode = timingMode
        self.run(animation)
    }
}

enum FormatError: Error {
    case wrongFormat(String)
}
