//
//  GameUI.swift
//  LightJump
//
//  Created by Nikola Bozhkov on 23.02.19.
//  Copyright © 2019 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class GameUI {
    
    static let comboMaxInactive: TimeInterval = 0.6
    static let comboTimeFrame: TimeInterval = 0.2
    
    var enemyKillTimePassed: TimeInterval = 0
    
    var lifeBar: SKSpriteNode!
    
    let stageLabel = SKLabelNode()
    let pointsLabel = SKLabelNode()
    let pointsGain: Int = 1
    var points: Int = 0 {
        didSet {
            pointsLabel.text = String(points)
        }
    }
    
    let comboLabel = SKLabelNode()
    
    var comboInactiveTimePassed: TimeInterval = 0
    var combo: Int = 0 {
        didSet {
            if combo > 1 {
                comboLabel.text = "\(combo)x"
                comboLabel.isHidden = false
            }
            else {
                comboLabel.isHidden = true
            }
        }
    }
    
    init(scene: SKScene) {
        lifeBar = SKSpriteNode(color: .red, size: CGSize(width: scene.frame.width, height: 30))
        lifeBar.anchorPoint = CGPoint(x: 0, y: 1)
        lifeBar.position = scene.point(left: 0, top: 0)
        pointsLabel.text = "0"
        pointsLabel.fontSize = 60
        pointsLabel.horizontalAlignmentMode = .center
        pointsLabel.verticalAlignmentMode = .top
        pointsLabel.position = CGPoint(x: scene.frame.midX, y: lifeBar.position.y - lifeBar.frame.height - 20)
        
        stageLabel.text = "Speed Level: 1"
        stageLabel.fontSize = 60
        stageLabel.horizontalAlignmentMode = .left
        stageLabel.verticalAlignmentMode = .top
        stageLabel.position = CGPoint(x: scene.left(20), y: pointsLabel.position.y - pointsLabel.frame.height - 20)
        
        comboLabel.text = "0x"
        comboLabel.isHidden = true
        comboLabel.fontSize = 60
        comboLabel.horizontalAlignmentMode = .left
        comboLabel.verticalAlignmentMode = .top
        comboLabel.position = stageLabel.position.offsetted(dx: 0, dy: -stageLabel.frame.height - 20)
        
        scene.addChild(lifeBar)
        scene.addChild(pointsLabel)
        scene.addChild(comboLabel)
        scene.addChild(stageLabel)
    }
    
    func didKillEnemy() {
        if enemyKillTimePassed <= GameUI.comboTimeFrame {
            combo += 1
            comboInactiveTimePassed = 0
        }
        
        enemyKillTimePassed = 0
        points += (combo + 1) * pointsGain
    }
    
    func update(deltaT: TimeInterval) {
        updateCombo(deltaT: deltaT)
    }
    
    func updateCombo(deltaT: TimeInterval) {
        enemyKillTimePassed += deltaT
        if enemyKillTimePassed > GameUI.comboTimeFrame {
            comboInactiveTimePassed += deltaT
        }
        
        if comboInactiveTimePassed > GameUI.comboMaxInactive {
            comboInactiveTimePassed = 0
            combo = 0
            enemyKillTimePassed = 0
        }
    }
    
    func lifeDidChange(_ newValue: CGFloat) {
        lifeBar.xScale = newValue / 100
    }
    
    func stageDidChange(_ newValue: CGFloat) {
        stageLabel.text = "Speed Level: \(Int(newValue))"
    }
}
