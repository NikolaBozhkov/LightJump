//
//  GameField+Extensions.swift
//  LightJump
//
//  Created by Nikola on 05/05/2018.
//  Copyright © 2018 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

extension GameField {
    var minX: CGFloat {
        return rect.minX
    }
    
    var midX: CGFloat {
        return rect.midX
    }
    
    var maxX: CGFloat {
        return rect.maxX
    }
    
    var minY: CGFloat {
        return rect.minY
    }
    
    var midY: CGFloat {
        return rect.midY
    }
    
    var maxY: CGFloat {
        return rect.maxY
    }
    
    var width: CGFloat {
        return rect.width
    }
    
    var height: CGFloat {
        return rect.height
    }
}
