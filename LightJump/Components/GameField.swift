//
//  GameField.swift
//  LightJump
//
//  Created by Nikola on 05/05/2018.
//  Copyright © 2018 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class GameField: SKNode, SKPhysicsContactDelegate {
    
    let rect: CGRect
    let pointTopLeft: SKShapeNode
    let pointTopRight: SKShapeNode
    let pointBottomLeft: SKShapeNode
    let pointBottomRight: SKShapeNode
    
    var points: [SKShapeNode] {
        return [pointTopLeft, pointTopRight, pointBottomLeft, pointBottomRight]
    }
    
    convenience init(size: CGSize) {
        let sideLength = size.width - ( GameField.margin + GameField.pointRadius) * 2
        self.init(rect: CGRect(x: GameField.margin + GameField.pointRadius,
                               y: size.height / 2 - sideLength / 2,
                               width: sideLength,
                               height: sideLength))
    }
    
    init(rect: CGRect = GameField.defaultRect) {
        self.rect = rect
        
        // Guide lines
        let guideLineWidth: CGFloat = 2
        let topGuide = SKSpriteNode(color: .white, size: CGSize(width: rect.width, height: guideLineWidth))
        topGuide.alpha = 0.3
        topGuide.position = CGPoint(x: rect.midX, y: rect.maxY)
        let bottomGuide = topGuide.copy() as! SKSpriteNode
        bottomGuide.position = CGPoint(x: rect.midX, y: rect.minY)
        let leftGuide = SKSpriteNode(color: .white, size: CGSize(width: guideLineWidth, height: rect.height))
        leftGuide.alpha = topGuide.alpha
        leftGuide.position = CGPoint(x: rect.minX, y: rect.midY)
        let rightGuide = leftGuide.copy() as! SKSpriteNode
        rightGuide.position = CGPoint(x: rect.maxX, y: rect.midY)
        let diagonalGuideUp = SKSpriteNode(color: .white, size: CGSize(width: guideLineWidth, height: rect.height * sqrt(2)))
        diagonalGuideUp.alpha = topGuide.alpha
        diagonalGuideUp.position = CGPoint(x: rect.midX, y: rect.midY)
        diagonalGuideUp.zRotation = -.pi / 4
        let diagonalGuideDown = diagonalGuideUp.copy() as! SKSpriteNode
        diagonalGuideDown.zRotation = .pi / 4
        
        func getPoint(position: CGPoint) -> SKShapeNode {
            let point = SKShapeNode(circleOfRadius: GameField.pointRadius)
            point.fillColor = .white
            point.position = position
            return point
        }
        
        // Create points
        pointTopLeft = getPoint(position: CGPoint(x: rect.minX, y: rect.maxY))
        pointTopRight = getPoint(position: CGPoint(x: rect.maxX, y: rect.maxY))
        pointBottomLeft = getPoint(position: CGPoint(x: rect.minX, y: rect.minY))
        pointBottomRight = getPoint(position: CGPoint(x: rect.maxX, y: rect.minY))
        
        super.init()
        addChild(leftGuide)
        addChild(rightGuide)
        addChild(bottomGuide)
        addChild(topGuide)
        addChild(diagonalGuideUp)
        addChild(diagonalGuideDown)
        addChild(pointTopLeft)
        addChild(pointTopRight)
        addChild(pointBottomLeft)
        addChild(pointBottomRight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension GameField {
    static let margin: CGFloat = 60
    static let pointRadius: CGFloat = 40
    static let sideLength: CGFloat = 750 - GameField.pointRadius * 2 - GameField.margin * 2
    static let defaultRect = CGRect(x: GameField.margin + GameField.pointRadius, y: 0,
                                    width: GameField.sideLength, height: GameField.sideLength)
}
