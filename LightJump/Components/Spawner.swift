import SpriteKit

class Spawner: EnemyDelegate {
    let spawnIntervalGainSec: TimeInterval = 0.02
    var spawnInterval: TimeInterval = 0.2
    
    unowned let scene: GameScene
    var timePassed: TimeInterval
    var spawnPoints: [CGPoint] = []
    var spawnPointsTaken: [CGPoint] = []
    
    init(scene: GameScene) {
        timePassed = spawnInterval
        self.scene = scene
        
        let countPerSide = 3
        let countPerDiagonal = 5
        let spacingSide = (GameField.sideLength - GameField.pointRadius * 2) / CGFloat(countPerSide + 1)
        let spacingDiagonal = (GameField.sideLength * sqrt(2) - GameField.pointRadius * 2) / CGFloat(countPerDiagonal + 1)
        
        for i in 1...countPerSide {
            let spacing = GameField.pointRadius + CGFloat(i) * spacingSide
            spawnPoints.append(CGPoint(x: scene.gameField.minX + spacing, y: scene.gameField.maxY))
            spawnPoints.append(CGPoint(x: scene.gameField.minX + spacing, y: scene.gameField.minY))
            spawnPoints.append(CGPoint(x: scene.gameField.maxX, y: scene.gameField.minY + spacing))
            spawnPoints.append(CGPoint(x: scene.gameField.minX, y: scene.gameField.minY + spacing))
        }
        
        for i in 1...countPerDiagonal {
            if i == countPerDiagonal / 2 + 1 { continue } // Skip the center
            let spacingX = GameField.pointRadius + (CGFloat(i) * spacingDiagonal - Enemy.radius) * cos(.pi / 4)
            let spacingY = GameField.pointRadius + (CGFloat(i) * spacingDiagonal - Enemy.radius) * sin(.pi / 4)
            spawnPoints.append(CGPoint(x: scene.gameField.minX + spacingX,
                                       y: scene.gameField.minY + spacingY))
            
            spawnPoints.append(CGPoint(x: scene.gameField.minX + spacingX,
                                       y: scene.gameField.maxY - spacingY))
        }
        
        spawnPoints.append(CGPoint(x: scene.gameField.midX, y: scene.gameField.midY))
    }
    
    func spawnEnemy(except: CGPoint? = nil) {
        let availablePoints = spawnPoints.filter { spawnPointsTaken.index(of: $0) == nil && $0 != except }
        let index = Int(arc4random_uniform(UInt32(availablePoints.count)))
        
        let enemy = Enemy(spawnPoint: availablePoints[index], lifespan: 2.5, isHealer: false)
        enemy.delegate = self
        scene.addChild(enemy)
        spawnPointsTaken.append(availablePoints[index])
    }
    
    func update(deltaT: TimeInterval) {
        timePassed += deltaT
        
        if timePassed > spawnInterval {
//            spawnEnemy()
            timePassed = 0
        }
        
        spawnInterval += spawnIntervalGainSec * deltaT
    }
    
    func didRemove(_ enemy: Enemy) {
        spawnPointsTaken.remove(at: spawnPointsTaken.index(of: enemy.spawnPoint)!)
    }
}
