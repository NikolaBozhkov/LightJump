//
//  EnemyDelegate.swift
//  LightJump
//
//  Created by Nikola on 05/05/2018.
//  Copyright © 2018 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

protocol EnemyDelegate {
    func didRemove(_ enemy: Enemy)
}
