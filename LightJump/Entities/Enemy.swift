//
//  Enemy.swift
//  LightJump
//
//  Created by Nikola on 05/05/2018.
//  Copyright © 2018 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class Enemy: SKShapeNode {
    static let bitMask: UInt32 = 0b10
    static let radius: CGFloat = 30
    static var diameter: CGFloat {
        return radius * 2
    }
    
    let spawnPoint: CGPoint
    var delegate: EnemyDelegate?
    
    init(spawnPoint: CGPoint, lifespan: TimeInterval = 4, isHealer: Bool = false) {
        self.spawnPoint = spawnPoint
        super.init()
        self.position = spawnPoint
        path = CGPath(ellipseIn: CGRect(x: -Enemy.radius, y: -Enemy.radius,
                                        width: Enemy.diameter, height: Enemy.diameter), transform: nil)
        fillColor = .cyan
        strokeColor = .cyan
        lineWidth = 1
        glowWidth = 2
        setScale(0)
        
        physicsBody = SKPhysicsBody(circleOfRadius: Enemy.radius)
        physicsBody!.categoryBitMask = Enemy.bitMask
        physicsBody!.contactTestBitMask = Player.bitMask
        physicsBody!.collisionBitMask = 0
        physicsBody!.isDynamic = false
        
        run(SKAction.sequence([
            SKAction.scale(to: 1, duration: 0.1),
            SKAction.run {
                self.physicsBody!.isDynamic = true
            }]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func removeFromParent() {
        super.removeFromParent()
        delegate?.didRemove(self)
    }
}
