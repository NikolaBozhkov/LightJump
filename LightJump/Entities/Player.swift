//
//  Player.swift
//  LightJump
//
//  Created by Nikola on 05/05/2018.
//  Copyright © 2018 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class Player: SKShapeNode {
    static let bitMask: UInt32 = 0b1
    static let radius: CGFloat = 12
    static var diameter: CGFloat {
        return radius * 2
    }
    
    let emitter = SKEmitterNode(fileNamed: "PlayerTrail")!
    
    init(position: CGPoint) {
        super.init()
        
        self.position = position
        path = CGPath(ellipseIn: CGRect(x: -Player.radius, y: -Player.radius,
                                        width: Player.diameter, height: Player.diameter), transform: nil)
        fillColor = .white
        lineWidth = 0
        
        physicsBody = SKPhysicsBody(circleOfRadius: Player.radius)
        physicsBody!.categoryBitMask = Player.bitMask
        physicsBody!.contactTestBitMask = Enemy.bitMask
        physicsBody!.collisionBitMask = 0
        
        addChild(emitter)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
